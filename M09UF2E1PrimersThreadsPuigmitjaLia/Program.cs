﻿using System;

namespace M09UF2E1PrimersThreadsPuigmitjaLia // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        private static int numLinies;


        static void Main(string[] args)
        {
            OneOnlyThread();
        }



        static void OneOnlyThread()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("cuento1");
            Thread myFirstThread = new Thread(ThreadAmbParametre);
            myFirstThread.Start("cuento1.txt");
            Thread.Sleep(1);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("cuento2");
            Thread mySecondThread = new Thread(ThreadAmbParametre);
            mySecondThread.Start("cuento2.txt");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("cuento3");
            Thread myThirdThread = new Thread(ThreadAmbParametre);
            myThirdThread.Start("cuento3.txt");

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("cuento4");
            Thread myFourThread = new Thread(ThreadAmbParametre);
            myFourThread.Start("cuento4.txt");
        }


        static void ThreadAmbParametre(object fitxer)
        {
            var fullPath = Path.GetFullPath(Directory.GetCurrentDirectory() + "/../"+ fitxer);
            numLinies = NumeroLiniesText(fullPath);
           // Console.WriteLine(numLinies);
            var prova = new Prova();

            prova.DalePruebaDale(numLinies);
        }


        private static int NumeroLiniesText(string fitxer)
        {

            string[] linies = File.ReadAllLines(fitxer);

            return linies.Length;
        }


    }
}