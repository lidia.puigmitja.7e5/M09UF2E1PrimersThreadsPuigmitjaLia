﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M09UF2E1PrimersThreadsPuigmitjaLia
{
    internal class Prova
    {
        private bool _runBool = false;


        public void DalePruebaDale(int linies)
        {
            NumeroDeThread(linies);
            // NumeroDeLinies(linies);
        }

        public void NumeroDeLinies(object linies)
        {
            //Object a array-string
            string liniesString = Convert.ToString(linies);
            //control per sortir del do-while
            int numInicial=0;
            int positioNumeroLinies = 0;
            do
            {
                if (numInicial == liniesString[positioNumeroLinies] && positioNumeroLinies < liniesString.Length)
                {
                    //x recorrer l'array
                    positioNumeroLinies++;
                    //per sortir del do-while
                    numInicial++;
                }
                else
                {
                    _runBool = true;
                }

            } while (_runBool != true);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"El conte té {linies} linies.");
            
        }

        public void NumeroDeThread(int linies)
        {

            Thread ThreadWithParam = new Thread(NumeroDeLinies);
            ThreadWithParam.Start(linies);

        }

    }
}
